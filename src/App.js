import React, { Component } from 'react';
import './App.css';

import { Segment,Grid,Container,Header,List,Divider,Image,Menu} from 'semantic-ui-react';
import { Route, Link } from "react-router-dom";
import { StickyContainer, Sticky } from 'react-sticky';


class App extends Component {


  constructor(props) {
    super(props);
    this.state = {
   
    }
  }


  rendernavigation=() =>{
 
    return (
      <div>
    
           
       <Menu pointing >
          
          <Menu.Item as='a' header as={Link} to="/home" >
          <Image size='mini' src='/logo.png' style={{ marginRight: '1.5em' }} />
          Project Name
          </Menu.Item>
         <Menu.Item as={Link} to="/drivers" name='Drivers' active={this.state.activeItem === 'Usuarios'} onClick={this.handleItemClick}/>
         <Menu.Item as={Link} to="/races" name='Racers' active={this.state.activeItem === 'Usuarios'} onClick={this.handleItemClick}/>  
      </Menu>


   <Segment inverted vertical style={{ margin: '45em 0em 0em', padding: '5em 0em' }}>
      <Container textAlign='center'>
        <Grid divided inverted stackable>
          <Grid.Row>
            <Grid.Column width={3}>
              <Header inverted as='h4' content='Group 1' />
              <List link inverted>
                <List.Item as='a'>Link One</List.Item>
                <List.Item as='a'>Link Two</List.Item>
                <List.Item as='a'>Link Three</List.Item>
                <List.Item as='a'>Link Four</List.Item>
              </List>
            </Grid.Column>
            <Grid.Column width={3}>
              <Header inverted as='h4' content='Group 2' />
              <List link inverted>
                <List.Item as='a'>Link One</List.Item>
                <List.Item as='a'>Link Two</List.Item>
                <List.Item as='a'>Link Three</List.Item>
                <List.Item as='a'>Link Four</List.Item>
              </List>
            </Grid.Column>
            <Grid.Column width={3}>
              <Header inverted as='h4' content='Group 3' />
              <List link inverted>
                <List.Item as='a'>Link One</List.Item>
                <List.Item as='a'>Link Two</List.Item>
                <List.Item as='a'>Link Three</List.Item>
                <List.Item as='a'>Link Four</List.Item>
              </List>
            </Grid.Column>
            <Grid.Column width={7}>
              <Header inverted as='h4' content='Footer Header' />
              <p>
                Extra space for a call to action inside the footer that could help re-engage users.
              </p>
            </Grid.Column>
          </Grid.Row>
        </Grid>

        <Divider inverted section />
        <Image centered size='mini' src='/logo.png' />
        <List horizontal inverted divided link>
          <List.Item as='a' href='#'>
            Site Map
          </List.Item>
          <List.Item as='a' href='#'>
            Contact Us
          </List.Item>
          <List.Item as='a' href='#'>
            Terms and Conditions
          </List.Item>
          <List.Item as='a' href='#'>
            Privacy Policy
          </List.Item>
        </List>
      </Container>
    </Segment>




</div>
      
    );
  }
    
    render() {
     
    return (
      <div>
        {this.rendernavigation()}
        <Route></Route>
        
      </div>
    )
  
  }
}



export default App;
